module.exports.GParallax = require('./classes/globalParallax').default;
module.exports.Nearby = require('./classes/nearby').default;
module.exports.FormControl = require('./classes/formControl').default;
module.exports.ModelsManager = require('./classes/modelsManager').default;
module.exports.utils = require('./utils');
module.exports.bootstraperMaps = require('./bootstraper/maps').default;
module.exports.bootstraperYoutube = require('./bootstraper/youtube').default;
