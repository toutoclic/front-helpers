module.exports.min = require('./min').default;
module.exports.max = require('./max').default;
module.exports.slice = require('./slice').default;
module.exports.combine = require('./combine').default;
module.exports.without = require('./without').default;
