![Logo](https://www.awstudio.fr/files/logo_5bf827234607f.png)

# Utils
`import utils from 'aw-front-helpers/utils';` to import

## dom
- `utils.dom.getClosest(element, selector)` to get a parent element
