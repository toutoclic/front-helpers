import Handlebars from 'handlebars/runtime';

var _compare = function(lvalue, rvalue, options) {

  // lvalue = parseFloat(lvalue.replace(',', '.'));
  if (arguments.length < 3)
    throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
  var operator = options.hash.operator || "==";
  var operators = {
    '==':       function(l,r) { return l == r; },
    '===':      function(l,r) { return l === r; },
    '!=':       function(l,r) { return l != r; },
    '<':        function(l,r) { return l < r; },
    '>':        function(l,r) { return l > r; },
    '<=':       function(l,r) { return l <= r; },
    '>=':       function(l,r) { return l >= r; },
    'typeof':   function(l,r) { return typeof l == r; }
  }

  if (!operators[operator])
    throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);
  var result = operators[operator](lvalue,rvalue);

  if( result ) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
}

var _formatDate = function(creation_date, locale, options) {
  var d = new Date(creation_date);
  return d.toLocaleDateString(`${locale}-${locale.toUpperCase()}`);
}

var _initialize = function(){
  Handlebars.registerHelper('compare', _compare);
  Handlebars.registerHelper('formatDate', _formatDate);
}

export default _initialize();
