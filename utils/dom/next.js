var getNext = function (el) {
  let next = el.nextSibling;
  if (next && next.nodeType === 3) {
    return getNext(next);
  }
  return next;
};

export default getNext;