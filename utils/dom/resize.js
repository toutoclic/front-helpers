export default () => {
  return "onorientationchange" in window ? "orientationchange" : "resize";
}