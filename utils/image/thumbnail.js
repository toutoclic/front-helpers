import drawImageIOSFix from './drawImage';
import resize from './resize';
import * as EXIF from 'exif-js';

export default (file, width, height, resizeMethod, fixOrientation, callback, crossOrigin = null) => {
  // Not using `new Image` here because of a bug in latest Chrome versions.
  // See https://github.com/enyo/dropzone/pull/226
  let img = document.createElement('img');

  if (crossOrigin) {
    img.crossOrigin = crossOrigin;
  }

  img.onload = () => {
    let loadExif = callback => callback(1);

    if ((typeof EXIF !== 'undefined' && EXIF !== null) && fixOrientation) {
      loadExif = callback =>
        EXIF.getData(img, function () {
          return callback(EXIF.getTag(this, 'Orientation'));
        });
    }

    return loadExif(orientation => {
      file.width = img.width;
      file.height = img.height;

      let resizeInfo = resize.call(this, file, width, height, resizeMethod);

      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');

      canvas.width = resizeInfo.trgWidth;
      canvas.height = resizeInfo.trgHeight;

      if (orientation > 4) {
        canvas.width = resizeInfo.trgHeight;
        canvas.height = resizeInfo.trgWidth;
      }

      switch (orientation) {
        case 2:
          // horizontal flip
          ctx.translate(canvas.width, 0);
          ctx.scale(-1, 1);
          break;
        case 3:
          // 180° rotate left
          ctx.translate(canvas.width, canvas.height);
          ctx.rotate(Math.PI);
          break;
        case 4:
          // vertical flip
          ctx.translate(0, canvas.height);
          ctx.scale(1, -1);
          break;
        case 5:
          // vertical flip + 90 rotate right
          ctx.rotate(0.5 * Math.PI);
          ctx.scale(1, -1);
          break;
        case 6:
          // 90° rotate right
          ctx.rotate(0.5 * Math.PI);
          ctx.translate(0, -canvas.height);
          break;
        case 7:
          // horizontal flip + 90 rotate right
          ctx.rotate(0.5 * Math.PI);
          ctx.translate(canvas.width, -canvas.height);
          ctx.scale(-1, 1);
          break;
        case 8:
          // 90° rotate left
          ctx.rotate(-0.5 * Math.PI);
          ctx.translate(-canvas.width, 0);
          break;
      }

      // This is a bugfix for iOS' scaling bug.
      drawImageIOSFix(ctx, img, resizeInfo.srcX != null ? resizeInfo.srcX : 0, resizeInfo.srcY != null ? resizeInfo.srcY : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, resizeInfo.trgX != null ? resizeInfo.trgX : 0, resizeInfo.trgY != null ? resizeInfo.trgY : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);

      let thumbnail = canvas.toDataURL('image/png');

      if (callback != null) {
        return callback(thumbnail, canvas);
      }
    });
  };

  if (callback != null) {
    img.onerror = callback;
  }

  return img.src = file.dataURL;
}