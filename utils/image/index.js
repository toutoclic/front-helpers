module.exports.detectVerticalSquash = require('./detectVerticalSquash').default;
module.exports.resize = require('./resize').default;
module.exports.thumbnail = require('./thumbnail').default;
module.exports.drawImage = require('./drawImage').default;
