import detectVerticalSquash from './detectVerticalSquash';

export default (ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) => {
  let vertSquashRatio = detectVerticalSquash(img);
  return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
};