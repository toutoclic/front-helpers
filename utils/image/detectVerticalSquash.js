export default (img) => {
  let iw = img.naturalWidth;
  let ih = img.naturalHeight;
  let canvas = document.createElement('canvas');
  canvas.width = 1;
  canvas.height = ih;
  let ctx = canvas.getContext('2d');
  ctx.drawImage(img, 0, 0);
  let { data } = ctx.getImageData(1, 0, 1, ih);


  // search image edge pixel position in case it is squashed vertically.
  let sy = 0;
  let ey = ih;
  let py = ih;
  while (py > sy) {
    let alpha = data[((py - 1) * 4) + 3];

    if (alpha === 0) {
      ey = py;
    } else {
      sy = py;
    }

    py = (ey + sy) >> 1;
  }
  let ratio = (py / ih);

  if (ratio === 0) {
    return 1;
  } else {
    return ratio;
  }
};