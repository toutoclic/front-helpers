export default (file, width, height, resizeMethod) => {
  let info = {
    srcX: 0,
    srcY: 0,
    srcWidth: file.width,
    srcHeight: file.height,
    trgWidth: null,
    trgHeight: null
  };

  let srcRatio = file.width / file.height;

  // Automatically calculate dimensions if not specified
  if ((width == null) && (height == null)) {
    width = info.srcWidth;
    height = info.srcHeight;
  } else if ((width == null)) {
    width = height * srcRatio;
  } else if ((height == null)) {
    height = width / srcRatio;
  }

  // Make sure images aren't upscaled
  width = Math.min(width, info.srcWidth);
  height = Math.min(height, info.srcHeight);

  let trgRatio = width / height;

  if ((info.srcWidth > width) || (info.srcHeight > height)) {
    // Image is bigger and needs rescaling
    if (resizeMethod === 'crop') {
      if (srcRatio > trgRatio) {
        info.srcHeight = file.height;
        info.srcWidth = info.srcHeight * trgRatio;
      } else {
        info.srcWidth = file.width;
        info.srcHeight = info.srcWidth / trgRatio;
      }
    } else if (resizeMethod === 'contain') {
      // Method 'contain'
      if (srcRatio > trgRatio) {
        height = width / srcRatio;
      } else {
        width = height * srcRatio;
      }
    } else {
      throw new Error(`Unknown resizeMethod '${resizeMethod}'`);
    }
  }

  info.srcX = (file.width - info.srcWidth) / 2;
  info.srcY = (file.height - info.srcHeight) / 2;

  info.trgWidth = width;
  info.trgHeight = height;

  return info;
}