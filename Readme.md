![Logo](https://www.awstudio.fr/files/logo_5bf827234607f.png)

# Front helpers
A library of functionalities for common tasks in Javascript / Sass.

## Table of contents
### Javascripts
  - **bootstraper** [See the documentation](https://gitlab.com/awstudio/front-helpers/tree/master/bootstraper/Readme.md)
    - maps | Allow the initialization of the Bootstraper Google Map
    - youtube | Allow the initialization of the Bootstraper Youtube Iframe
  - **classes** [See the documentation](https://gitlab.com/awstudio/front-helpers/tree/master/classes/Readme.md)
    - formControl | A Javascript form validation class
    - globalParallax | A Parallax animation library
    - modelsManager | A RESTful HTTP client
    - nearby | A RESTful HTTP client
  - **utils** [See the documentation](https://gitlab.com/awstudio/front-helpers/tree/master/utils/Readme.md)
    - array
    - dom
    - func
    - math
    - media
    - regex
    - helpers

### stylesheets
Using [Sass](http://sass-lang.com/)

[See the documentation](https://gitlab.com/awstudio/front-helpers/tree/master/stylesheets/Readme.md)

  - functions | General mixins
  - medias | Media queries variables and mixins
  - mixins | Useful SASS mixins
  - reset | Reduce browser inconsistencies
