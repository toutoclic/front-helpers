import 'gsap';
import 'ScrollToPlugin';
import domselect from 'dom-select';
import classes from 'dom-classes';

import getClosest from '../utils/dom/getClosest';
import regex_email from '../utils/regex/email';

class FormControl {

  //________________________________________________________ constructor
  // -
  constructor(el) {
    this._el = el;
    this._dom = {};
    this._parentElementClass = null;
    this._formCreateBound();
    this._el.setAttribute('novalidate', 'novalidate');

    this._formGrabDom();
    this._formAddEvents();
  }

  //________________________________________________________ public
  // -
  destroy() {
    this._el.removeEventListener('submit', this._onFormSubmit);
    for (let i = 0; i < this._domForm.inputs.length; i++) {
      this._domForm.inputs[i].removeEventListener('keyup', this._onFormKeyUp);
      if (this._domForm.inputs[i].getAttribute('type') === 'checkbox' ||
        this._domForm.inputs[i].getAttribute('type') === 'radio' ||
        this._domForm.inputs[i].tagName === 'SELECT') {
        this._domForm.inputs[i].removeEventListener('change', this._onFormKeyUp);
      }
    }
  }
  //________________________________________________________ events
  // -
  _onFormSubmit(pEvt) {
    if (!this._formValidate()) {
      pEvt.preventDefault();
    }
  }

  _onFormKeyUp(pEvt) {
    var el = pEvt.target,
      parent = getClosest(el, this._parentElementClass);
    classes.remove(parent, 'is-error');
    classes.remove(parent, 'is-valid');
  }

  //________________________________________________________ private
  // -
  _formCreateBound() {
    ['_onFormKeyUp', '_onFormSubmit']
      .forEach((fn) => this[fn] = this[fn].bind(this));
  }

  _formGrabDom() {
    this._domForm = {};
    let texts = domselect.all('input[type=text]', this._el),
      emails = domselect.all('input[type=email]', this._el),
      tels = domselect.all('input[type=tel]', this._el),
      passwords = domselect.all('input[type=password]', this._el),
      textarea = domselect.all('textarea', this._el),
      radios = domselect.all('input[type=radio]', this._el),
      checkbox = domselect.all('input[type=checkbox]', this._el),
      numbers = domselect.all('input[type=number]', this._el),
      selects = domselect.all('select', this._el);
    this._domForm.inputs = [...texts, ...emails, ...passwords, ...tels, ...numbers, ...checkbox, ...radios, ...selects, ...textarea];
  }

  _formValidate() {
    let validate = true,
      errorEl = null;
    for (let i = 0; i < this._domForm.inputs.length; i++) {
      let el = this._domForm.inputs[i],
        parent = getClosest(el, this._parentElementClass);
      if (parent === null) continue;
      classes.remove(parent, 'is-error');
      if (el.getAttribute('type') && el.getAttribute('type') === 'email' && !regex_email(el.value)) {
        classes.add(parent, 'is-error');
        validate = false;
      }
      if (el.getAttribute('data-formvalidate-same') && domselect(`[data-formvalidate-same=${el.getAttribute('data-formvalidate-same')}]`).value !== el.value) {
        classes.add(parent, 'is-error');
        validate = false;
      }
      if (el.getAttribute('required') !== null) {
        if (el.getAttribute('type') === 'radio') {
          let isChecked = false;
          let els = domselect.all(`input[type="radio"][name="${el.getAttribute('name')}"]`, this._el);
          for (let i = 0; i < els.length; i++) {
            if (els[i].checked) {
              isChecked = true;
              break;
            }
          }
          if (!isChecked) {
            classes.add(parent, 'is-error');
            validate = false;
          }
        }
        if (el.getAttribute('type') === 'checkbox' && !el.checked) {
          classes.add(parent, 'is-error');
          validate = false;
        } else if (el.value === '') {
          classes.add(parent, 'is-error');
          validate = false;
        }
      }
      if (!validate && errorEl === null) {
        errorEl = el;
      }
    }
    if (!validate && errorEl) {
      TweenMax.to(window, .6, { scrollTo: (window.pageYOffset + errorEl.getBoundingClientRect().top - 150) })
    }
    return validate;
  }

  _formAddEvents() {
    this._el.addEventListener('submit', this._onFormSubmit);
    for (let i = 0; i < this._domForm.inputs.length; i++) {
      this._domForm.inputs[i].addEventListener('keyup', this._onFormKeyUp);
      if (this._domForm.inputs[i].getAttribute('type') === 'checkbox' ||
        this._domForm.inputs[i].getAttribute('type') === 'radio' ||
        this._domForm.inputs[i].tagName === 'SELECT') {
        this._domForm.inputs[i].addEventListener('change', this._onFormKeyUp);
      }
    }
  }
}

export default FormControl;
