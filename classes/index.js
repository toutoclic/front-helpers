module.exports.GParallax = require('./globalParallax').default;
module.exports.Nearby = require('./nearby').default;
module.exports.FormControl = require('./formControl').default;
module.exports.ModelsManager = require('./modelsManager').default;