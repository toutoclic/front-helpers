import axios from 'axios';

class ModelsManager {

  //________________________________________________________ constructor
  // -
  constructor() {
    this._url = null;
    this._datas = null;
    this._limit = null;
    this._instance = axios.create();
    this._source = axios.CancelToken.source();
  }

  //________________________________________________________ public
  // -
  get model() {
    return this._datas;
  }

  get(id) {
    return this._instance
      .get(`${this._url}/${id}`)
      .then(response => {
        this._datas = this._parseDatas(response.data);
        return response.data;
      })
      .catch(err => {
        console.log('ERROR', 'modelsManager', 'get', this.constructor.name);
        throw Error(err.response.data);
      });
  }

  getAll(params = null) {
    this._source = axios.CancelToken.source();

    let url = this._url;
    if (params)
      url += this._parseUrl(params);

    return this._instance
      .get(url, { cancelToken: this._source.token })
      .then(response => {
        this._datas = this._parseDatas(response.data);
        return response.data;
      })
      .catch(err => {
        console.log('ERROR', 'modelsManager', 'add', this.constructor.name);
        if (err.response)
          throw Error(err.response.data);
        else return {};
      });
  }

  add(datas) {
    return this._instance
      .post(this._url, datas)
      .then(response => {
        this._datas = this._parseDatas(response.data);
        return response.data;
      })
      .catch(err => {
        console.log('ERROR', 'modelsManager', 'add', this.constructor.name);
        throw Error(err.response.data);
      });
  }

  put(id, datas) {
    return this._instance
      .put(`${this._url}/${id}`, datas)
      .then(response => {
        this._datas = this._parseDatas(response.data);
        return response.data;
      })
      .catch(err => {
        console.log('ERROR', 'modelsManager', 'put', this.constructor.name);
        throw Error(err.response.data);
      });
  }

  patch(id, datas) {
    return this._instance
      .patch(`${this._url}/${id}`, datas)
      .then(response => {
        this._datas = this._parseDatas(response.data);
        return response.data;
      })
      .catch(err => {
        console.log('ERROR', 'modelsManager', 'patch', this.constructor.name);
        throw Error(err.response.data);
      });
  }

  delete(id) {
    return this._instance
      .delete(`${this._url}/${id}`)
      .then(response => {
        this._datas = this._parseDatas(response.data);
        return response.data;
      })
      .catch(err => {
        console.log('ERROR', 'modelsManager', 'delete', this.constructor.name);
        throw Error(err.response.data);
      });
  }

  cancel() {
    this._source.cancel();
  }

  //________________________________________________________ private
  // -
  _parseDatas(datas) {
    return datas;
  }

  _parseUrl(params) {
    return `?${Object.keys(params).map(key => key + '=' + params[key]).join('&')}`;
  }
}

export default ModelsManager;
