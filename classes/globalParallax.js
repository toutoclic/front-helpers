import Symbol from 'es6-symbol';
import EventEmitter from 'events';
import classes from 'dom-classes';
import raf from 'raf';

// singleton
let singleton = Symbol();
let singletonEnforcer = Symbol();
const SIMPLE_REFENCE = .5;
const TABLET_WIDTH = 768;

class GParallax extends EventEmitter {
  //________________________________________________________ constructor
  // -
  constructor(pEnforcer) {
    if (pEnforcer !== singletonEnforcer) {
      throw 'Global Parallax: Cannot construct singleton';
    }
    super();
    this._raf = null;
  }

  static getInstance() {
    if (!this[singleton]) {
      this[singleton] = new GParallax(singletonEnforcer);
    }
    return this[singleton];
  }

  //________________________________________________________ public
  // -

  start() {
    if (this._domItems || this._bgItems) return;
    this._isResizing = false;
    this._initialized = false;

    this._vars = {
      height: 0,
      width: 0,
      scrollY: 0,
      ease: 1
    };

    this._onGrab();
    this._specialCases = [];

    this._createBound();
    this._onResize();
    this._addListeners();

    setTimeout(this._onScroll);
  }

  //________________________________________________________ events
  // -

  _onResize() {
    this._isResizing = true;

    this._vars.scrollY = window.pageYOffset;
    this._vars.width = document.documentElement.clientWidth || window.innerWidth;
    this._vars.height = document.documentElement.clientHeight || window.innerHeight;

    this._resetItemsCached();
    this._resetBgCached();
    this._getCache();
    this._isResizing = false;
  }

  _onScroll() {
    this._vars.scrollY = window.pageYOffset;
    this._raf = raf(this._update);
  }

  _update() {
    if (!this) return;
    if (this._isResizing) return;
    this._updateBackgrounds();
    this._updateItems();
  }

  _onGrab() {
    const items = Array.prototype.slice.call(document.body.querySelectorAll('[data-prllx-item]', 0));
    const itemsSimple = Array.prototype.slice.call(document.body.querySelectorAll('[data-prllx-aw]', 0));

    this._domItems = [...items, ...itemsSimple];
    this._domBgItems = Array.prototype.slice.call(document.body.querySelectorAll('[data-prllx-bg]', 0));

    const countImage = this._domItems.length;
    let imgCount = 0;
    this._domItems.forEach((img) => {
      if (typeof img.src === 'undefined') return;
      const image = new Image();
      image.onload = () => {
        imgCount++;
        if (countImage === imgCount) this._onResize();
      };
      image.onerror = () => {
        imgCount++;
        if (countImage === imgCount) this._onResize();
      };
      image.src = img.src;
    });
  }

  //________________________________________________________ private methods
  // -

  _createBound() {
    ['_onResize', '_onScroll', '_calculateViewport', '_update', '_onGrab']
      .forEach((fn) => this[fn] = this[fn].bind(this));
  }

  _addListeners() {
    window.addEventListener('scroll', this._onScroll);
    window.addEventListener('resize', this._onResize);

    document.body.addEventListener('page::heightChange', this._onResize);
    document.body.addEventListener('page::getElements', this._onGrab);
  }

  _removeListeners() {
    window.removeEventListener('scroll', this._onScroll);
    window.removeEventListener('resize', this._onResize);
  }

  _resetItemsCached() {
    this._domItems.forEach((el) => {
      el.style.transform =
        el.style['-moz-transform'] =
        el.style['-ms-transform'] =
        el.style['-webkit-transform'] = `translate3d(0, 0, 0)`;
      classes.remove(el, 'u-hidden');
    });
  }

  _resetBgCached() {
    this._domBgItems.forEach((el) => {
      el.style.transform =
        el.style['-moz-transform'] =
        el.style['-ms-transform'] =
        el.style['-webkit-transform'] = `translate3d(0, 0, 0)`;
      el.style.height = '100%';
      el.style.top = '0%';
    });
  }

  _getCache() {
    this._cachedItems = [];
    this._cachedBackgrounds = [];
    this._domItems.forEach((el) => { this._cachedItems.push(this._createCacheItem(el)); });
    this._domBgItems.forEach((el) => {
      const cache = this._createCacheItem(el);
      let delta = (cache.deltaEnd - cache.deltaStart) * 100 / 2;
      let top = (cache.deltaStart * 100 / 2).toFixed(0);
      (delta < 0) && (delta = -delta, top = -top);
      const height = 100 + delta;
      cache.el.style.height = `${height}%`;
      cache.el.style.top = `${top}%`;
      this._cachedBackgrounds.push(cache);
    });
  }

  _getCacheForSpecialCases() {
    this._cachedSpecialCases = [];
    this._specialCases.forEach((el) => { this._cachedSpecialCases.push(this._createCacheItem(el)); });
  }

  _createCacheItem(domEl) {
    const bounding = domEl.getBoundingClientRect();
    const options = {
      el: domEl,
      top: bounding.top + this._vars.scrollY,
      bottom: bounding.bottom + this._vars.scrollY,
      height: bounding.height,
      type: domEl.getAttribute('data-prllx-item'),
      speed: domEl.getAttribute('data-speed') || 1,
      deltaStart: domEl.getAttribute('data-delta-start') || 0,
      deltaEnd: domEl.getAttribute('data-delta-end') || 0,
      invert: domEl.hasAttribute('data-invert') || false,
      state: true
    }

    const elSimple = domEl.getAttribute('data-prllx-aw');
    if (elSimple !== null) {
      if (elSimple === 'top') {
        options.deltaStart = SIMPLE_REFENCE;
        options.deltaEnd = -SIMPLE_REFENCE;
      } else if (elSimple === 'bottom') {
        options.deltaStart = -SIMPLE_REFENCE;
        options.deltaEnd = SIMPLE_REFENCE;
      }
    }
    return options;
  }

  _updateItems() {
    if (this._vars.width < TABLET_WIDTH) return;
    this._cachedItems.forEach((cacheItem) => {
      const testo = this._calculateViewport(cacheItem);

      if (testo.isVisible || !this._initialized) {
        !cacheItem.state && classes.remove(cacheItem.el, 'u-hidden');
        cacheItem.el.style.transform =
          cacheItem.el.style['-moz-transform'] =
          cacheItem.el.style['-ms-transform'] =
          cacheItem.el.style['-webkit-transform'] = testo.type === 'hz' ? `translate3d(${testo.value}px, 0, 0)` : `translate3d(0, ${testo.value}px, 0)`;
      } else if (cacheItem.state) {
        classes.add(cacheItem.el, 'u-hidden');
      }

      cacheItem.state = testo.isVisible;
    });
    this._initialized = true;
  }

  _updateBackgrounds() {
    this._cachedBackgrounds.forEach((cacheItem) => {
      const testo = this._calculateBgViewport(cacheItem);
      if (testo.isVisible) {
        cacheItem.el.style.transform =
          cacheItem.el.style['-moz-transform'] =
          cacheItem.el.style['-ms-transform'] =
          cacheItem.el.style['-webkit-transform'] = `translate3d(0, ${testo.value}px, 0)`;
      }
    });
  }

  _calculateViewport(cache) {
    if (this._isResizing) return;
    const speed = cache.speed;

    const deltaStart = cache.height * cache.deltaStart;
    const deltaEnd = cache.height * cache.deltaEnd;
    const delta = deltaEnd - deltaStart;

    const top = cache.top + deltaStart;
    const bottom = cache.bottom + deltaEnd;
    const hh = bottom - top;
    const fullHeight = hh + this._vars.height;

    const scrollY = this._vars.scrollY;
    const itemScrollY = (top - scrollY);

    const min = this._vars.height;

    const max = - hh;
    const ratio = (itemScrollY - min) / (max - min) * speed;

    const value = (deltaStart + delta * ratio) + (- fullHeight + fullHeight / speed) * ratio;
    const isVisible = ratio > -1 && ratio < 2;

    return { isVisible: isVisible, value: value.toFixed(2) };
  }

  _calculateBgViewport(cache) {

    const speed = cache.speed;

    const deltaStart = cache.height * cache.deltaStart / 2;
    const deltaEnd = cache.height * cache.deltaEnd / 2;
    const delta = deltaEnd - deltaStart;

    const top = cache.top;
    const bottom = cache.bottom;
    const hh = bottom - top;

    const scrollY = this._vars.scrollY;
    const itemScrollY = (top - scrollY);

    const min = this._vars.height;
    const max = - hh;

    const ratio = (itemScrollY - min) / (max - min);
    const isVisible = ratio > -1 && ratio < 2;
    const value = cache.invert ? (scrollY * speed) : (deltaStart + delta * ratio);

    return { isVisible: isVisible, value: value.toFixed(0) };
  }
}

export default GParallax.getInstance();
