![Logo](https://www.awstudio.fr/files/logo_5bf827234607f.png)

# classes
## formControl
Based on HTML5 basic options for inputs (required, type...).

- `import FormControl from 'aw-front-helpers/classes/formControl';` to import
- `class MyClass extends FormControl` to extend the formControl class to your class
- You must define `this._parentElementClass`: Defines a wrapper with an input and an error message

## globalParallax
- `import GParallax from 'aw-front-helpers/classes/globalParallax';` to import
- `GParallax.start()` to instantiate
- in your HTML element, you need define some atribute
  - `data-prllx-item` with option `data-speed` and `data-delta-start` (speed between 0 and 2): HTML element, moves vertically
  - `data-prllx-item-hz` with option `data-speed` and `data-delta-start` (speed between 0 and 2): HTML element, moves horizontally
  - `data-prllx-bg` with options `data-delta-start` and `data-delta-end`: defines a background-image to an element

## modelsManager
- `import ModelsManager from 'aw-front-helpers/classes/modelsManager';` to import
- `class MyModule extends ModelsManager` to extend

## nearby
- `import nearby from 'aw-front-helpers/classes/nearby';` to import
- `new Nearby(this._el, {options})`;
