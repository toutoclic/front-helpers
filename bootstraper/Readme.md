![Logo](https://www.awstudio.fr/files/logo_5bf827234607f.png)

# Bootstraper
## maps
- `import bootstraperMaps from 'aw-front-helpers/bootstraper/maps';` to import
- `bootstraperMaps(googleKeyApi).then( initMap )` to initialize
- you can specified libraries to load in second parameters `bootstraperMaps(googleKeyApi, [library, library]).then( initMap )` [Libraries](https://developers.google.com/maps/documentation/javascript/libraries)
  - drawing
  - geometry
  - places
  - visualization

## youtube
- `import bootstraperYT from 'aw-front-helpers/bootstraper/youtube';` to import
- `bootstraperYT().then( initPlayer )` to initialize