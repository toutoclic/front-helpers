import Symbol from 'es6-symbol/polyfill';

// singleton
const singleton = Symbol();
const singletonEnforcer = Symbol();

class bootstraperYoutube {
  //________________________________________________________ constructor
  // -
  constructor(pEnforcer) {
    if (pEnforcer !== singletonEnforcer) {
      throw 'Cannot construct singleton';
    }
    this._initialized = false;
    this._promise = new Promise((resolve, reject) => {
      this._resolve = resolve;
    });
  }

  static getInstance() {
    if (!this[singleton]) {
      this[singleton] = new bootstraperYoutube(singletonEnforcer);
    }
    return this[singleton];
  }

  _init() {
    let tag = document.createElement('script'),
      firstScriptTag = document.getElementsByTagName('script')[0];
    tag.src = 'https://www.youtube.com/iframe_api';
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  load() {
    window.onYouTubeIframeAPIReady = () => {
      this._initialized = true;
      this._resolve();
    };

    if (this._initialized) this._resolve();
    else this._init();

    return this._promise;
  }
}
export default bootstraperYoutube.getInstance();