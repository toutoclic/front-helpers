import Symbol from 'es6-symbol/polyfill';

// singleton
const singleton = Symbol();
const singletonEnforcer = Symbol();

class bootstraperMaps {
  //________________________________________________________ constructor
  // -
  constructor(pEnforcer) {
    if (pEnforcer !== singletonEnforcer) {
      throw 'Cannot construct singleton';
    }
    this._initialized = false;
    this._promise = new Promise((resolve, reject) => {
      this._resolve = resolve;
    });
  }

  static getInstance() {
    if (!this[singleton]) {
      this[singleton] = new bootstraperMaps(singletonEnforcer);
    }
    return this[singleton];
  }

  _init() {
    let tag = document.createElement('script'),
      firstScriptTag = document.getElementsByTagName('script')[0],
      libs = this._libraries ? `&libraries=${this._libraries.join(',')}` : ``;
    tag.src = `https://maps.googleapis.com/maps/api/js?key=${this._googleAPIKey}${libs}&callback=onMapsReady`;
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  load(googleAPIKey, libraries = null) {
    this._googleAPIKey = googleAPIKey;
    this._libraries = libraries;

    window.onMapsReady = () => {
      this._initialized = true;
      this._resolve();
    };
    if (this._initialized) this._resolve();
    else this._init();

    return this._promise;
  }
}
export default bootstraperMaps.getInstance();