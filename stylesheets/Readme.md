![Logo](https://www.awstudio.fr/files/logo_5bf827234607f.png)

# Stylesheets
## functions.scss
**Variables**
- px-base

**Functions**
- rem
- em

## medias.scss
**Variables**
- $breakpoint-phablet
- $breakpoint-tablet
- $breakpoint-desktop
- $breakpoint-desktop-header
- $breakpoint-desktop-xl

**Mixins**
- mobile
- phablet
- tablet
- tablet-max
- tablet-only
- desktop-header
- desktop
- desktop-xl

## mixins.scss
**Variables:**
- $wrapper-max
- $wrapper-width-sm
- $wrapper-width-md
- $wrapper-width-lg
- $vertical-spacing-mobile
- $vertical-spacing-tablet
- $vertical-spacing-desktop

**Mixins**
- wrapper
- vertical-spacing
  - vertical-spacing-top
  - vertical-spacing-bottom
- vertical-spacing-inner
  - vertical-spacing-inner-top
  - vertical-spacing-inner-bottom
- resetList
- resetBtn
- size
- centerBlock
- absolute
- fixed
- hidden
- visible
- unselectable
- clearfix
- imageCover
